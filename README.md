Trustful Finance is a financial services company offering three products:

- Checking and savings bank accounts
- Credit card
- Loans

The products are implemented with several systems:

- A website, an iOS app, and an Android app where customers use the products.
- A backend service which performs the finance logic and connects to a database. The website and mobile apps connect to this service via an API.
- A backend service which manages customer user accounts and connects to a database. The website and mobile apps connect to this service via an API.

```mermaid
graph TD;
  Website-->FinanceLogic;
  iOS-->FinanceLogic;
  Android-->FinanceLogic;
  Website-->UserAccounts;
  iOS-->UserAccounts;
  Android-->UserAccounts;
```
